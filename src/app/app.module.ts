import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { SensorComponent } from './sensor1/sensor.component';
import { Sensor2Component } from './sensor2/sensor2.component';
import { Sensor3Component } from './sensor3/sensor3.component';
import { Sensor4Component } from './sensor4/sensor4.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    SensorComponent,
    Sensor2Component,
    Sensor3Component,
    Sensor4Component
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
