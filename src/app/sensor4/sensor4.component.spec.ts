import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Sensor4Component } from './sensor4.component';

describe('Sensor4Component', () => {
  let component: Sensor4Component;
  let fixture: ComponentFixture<Sensor4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Sensor4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Sensor4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
