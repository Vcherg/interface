import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'int-sensor3',
  templateUrl: './sensor3.component.html',
  styleUrls: ['../sensor1/sensor.component.scss']
})
export class Sensor3Component implements OnInit,AfterViewInit {
  public editMode=false;
  public temperatureValue;
  public humidityValue;
  public pressureValue;
  public sensorName='Sensor 03';
  public tempUnit=' °C';
  public humUnit=' %';
  public presUnit=' Pa';
  @ViewChild('tempCanvas')tempCanvas:ElementRef;
  @ViewChild('humidityCanvas')humidityCanvas:ElementRef;
  @ViewChild('pressureCanvas')pressureCanvas:ElementRef;
  @ViewChild('tmpName')tmpName:ElementRef;
  constructor() {

  }
  confirm(){
    let name=this.tmpName
    this.sensorName=name.nativeElement.value;
    this.editMode=false;
  }
  drawer(ctxName){

    ctxName.beginPath();
    ctxName.lineWidth=20;
    ctxName.strokeStyle='#d0d0d0';
    ctxName.arc(ctxName.canvas.width*0.5,ctxName.canvas.height,ctxName.canvas.width*0.45,0,2*Math.PI,false);
    ctxName.stroke();
    ctxName.closePath();

  }

  drawArrow(ctxName,kat){
    ctxName.beginPath();
    ctxName.lineWidth=15;
    if(kat<=0.2){
      ctxName.strokeStyle='blue';
    }else if(kat>0.2&&kat<=0.8){
      ctxName.strokeStyle='green';
    }else{
      ctxName.strokeStyle='red';
    }
    //tempCtx.arc(presCtx.canvas.width*0.5,presCtx.canvas.height,presCtx.canvas.width*0.45,0,0.35*Math.PI,true);
    ctxName.arc(ctxName.canvas.width*0.5,ctxName.canvas.height,ctxName.canvas.width*0.45,Math.PI,(1+kat)*Math.PI,false);
    ctxName.stroke();
    ctxName.closePath();

  }

  prepareCanvas(){
    let tempCtx: CanvasRenderingContext2D = this.tempCanvas.nativeElement.getContext('2d');
    let humCtx: CanvasRenderingContext2D = this.humidityCanvas.nativeElement.getContext('2d');
    let presCtx: CanvasRenderingContext2D = this.pressureCanvas.nativeElement.getContext('2d');
    if(document.body.clientWidth>768){
      tempCtx.canvas.width=document.body.clientWidth*0.23;
      tempCtx.canvas.height=document.body.clientHeight*0.25;


      humCtx.canvas.width=document.body.clientWidth*0.23;
      humCtx.canvas.height=document.body.clientHeight*0.25;

      presCtx.canvas.width=document.body.clientWidth*0.23;
      presCtx.canvas.height=document.body.clientHeight*0.25;
    }else{
      tempCtx.canvas.width=document.body.clientWidth*0.31;
      tempCtx.canvas.height=document.body.clientHeight*0.1;


      humCtx.canvas.width=document.body.clientWidth*0.31;
      humCtx.canvas.height=document.body.clientHeight*0.1;

      presCtx.canvas.width=document.body.clientWidth*0.31;
      presCtx.canvas.height=document.body.clientHeight*0.1;
    }

    this.drawer(tempCtx);
    this.drawer(humCtx);
    this.drawer(presCtx);

  };

  ngOnInit(){
    this.prepareCanvas();
  }
  toPercents(val,max){
    return ((val*100)/max)/100
  }
  ngAfterViewInit() {
    var tempCtx: CanvasRenderingContext2D = this.tempCanvas.nativeElement.getContext('2d');
    var humCtx: CanvasRenderingContext2D = this.humidityCanvas.nativeElement.getContext('2d');
    var presCtx: CanvasRenderingContext2D = this.pressureCanvas.nativeElement.getContext('2d');
    var that=this;
    setInterval(function () {
      that.temperatureValue=Math.floor((Math.random() * 120) + 100);//min 1 max 120
      that.humidityValue=Math.floor((Math.random() * 10)+90);//max 100 min 0
      that.pressureValue=Math.floor((Math.random() * 2) + 9);//max 10 min 1
      that.prepareCanvas();
      that.drawArrow(tempCtx,that.toPercents(that.temperatureValue,120));
      that.drawArrow(presCtx,that.toPercents(that.pressureValue,10));
      that.drawArrow(humCtx,that.toPercents(that.humidityValue,100));
    },1000);

  }
}
