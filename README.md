# Interface v1.0
 
Projekt interface z kontrolkami czujników

[Live Projektu dostępny tu](https://interface-57106.firebaseapp.com)

##### Wykorzsytane technologie:

* Angular

* SASS

* HTML

##### Funkcjonalność

* Automatyczne generowanie wykresów na podstawie zadanej wartości

* Responsywność

* Brak frameworka gaficznego (mniejszy rozmiar aplikacji)

* Zmiana paratetrów czujników w kodzie

* Zmiana nazwy czujnika z poziomu użytkownika

##### Uruchamianie projektu do testu

1. Wpisać `ng serve` aby testować na komputerze lokalnym lub `ng serve --host twojAdresIP` 

##### Tworzenie wersji finalnej projektu

1. W pliku `./src/environments/environment.ts` zmienić wartość pola `production` na `true` (u mnie linia 7)

2. Wpisać ` npm run buildme` i cierpliwie czekać

##### Testy

Przetestowano na urządzeniach:

* Xiaomi Redmi Note 3 Pro

* Apple iPad2

* ASUS K52JT (Linux 16.04 LTS)


